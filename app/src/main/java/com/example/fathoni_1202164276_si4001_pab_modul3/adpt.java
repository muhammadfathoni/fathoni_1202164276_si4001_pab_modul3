package com.example.fathoni_1202164276_si4001_pab_modul3;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class adpt extends RecyclerView.Adapter<adpt.viewHolder>{
    ArrayList<data> listData = new ArrayList<>();
    private adpt.onclick listener;

    interface onclick{
        void diklik(data saatIni);
    }

    public adpt(ArrayList<data> listData, adpt.onclick listener) {
        this.listData = listData;
        this.listener = listener;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new viewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.per_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder viewHolder, final int position) {
        viewHolder.name.setText(listData.get(position).getNama());
        viewHolder.job.setText(listData.get(position).getPekerjaan());
        if (listData.get(position).getJenisKelamin().equals("Male")){
            viewHolder.gender.setImageResource(R.drawable.male);
        }else if (listData.get(position).getJenisKelamin().equals("Female")){
            viewHolder.gender.setImageResource(R.drawable.female);
        }

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { listener.diklik(listData.get(position)); }
        });
    }

    public  void addItem(data baru){
        listData.add(baru);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() { return listData.size(); }

    class viewHolder extends RecyclerView.ViewHolder{
        ImageView gender;
        TextView name, job;
        public viewHolder(@NonNull View itemView) {
            super(itemView);
            gender = itemView.findViewById(R.id.gambar_per_item);
            name = itemView.findViewById(R.id.nama_per_item);
            job = itemView.findViewById(R.id.pekerjaan_per_item);
        }
    }
}
