package com.example.fathoni_1202164276_si4001_pab_modul3;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;


import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    RecyclerView rec;
    ArrayList<com.example.fathoni_1202164276_si4001_pab_modul3.data> data = new ArrayList<>();
    adpt adpt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rec = findViewById(R.id.data);
        rec.setHasFixedSize(true);

        adpt = new adpt(data, new adpt.onclick() {
            @Override
            public void diklik(com.example.fathoni_1202164276_si4001_pab_modul3.data saatIni) {
                Intent blabla = new Intent(MainActivity.this, dtl.class);
                blabla.putExtra("nama", saatIni.getNama());
                blabla.putExtra("job", saatIni.getPekerjaan());
                blabla.putExtra("jeniskelamin", saatIni.getJenisKelamin());
                startActivity(blabla);
            }
        });

        rec.setLayoutManager(new LinearLayoutManager(this));
        rec.setAdapter(adpt);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation==Configuration.ORIENTATION_LANDSCAPE){
            rec.setLayoutManager(new GridLayoutManager(this, 2));
        }else if (newConfig.orientation==Configuration.ORIENTATION_PORTRAIT){
            rec.setLayoutManager(new LinearLayoutManager(this));
        }
    }

    public void tambahdata(View view) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
        final View dialogView = getLayoutInflater().inflate(R.layout.adduser, null);
        dialog.setView(dialogView);
        dialog.setCancelable(true);
        dialog.setIcon(R.mipmap.ic_launcher);
        dialog.setTitle("Create new user");
        final AlertDialog dlg = dialog.create();

        (dialogView.findViewById(R.id.onsubmit)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(MainActivity.class.getSimpleName(), ((Spinner)dialogView.findViewById(R.id.jk)).getSelectedItem().toString());
                adpt.addItem(new data(
                        ((EditText)dialogView.findViewById(R.id.userbaru)).getText().toString(),
                        ((EditText)dialogView.findViewById(R.id.pkj)).getText().toString(),
                        ((Spinner)dialogView.findViewById(R.id.jk)).getSelectedItem().toString()

                ));

                dlg.dismiss();
            }
        });

        (dialogView.findViewById(R.id.oncancel)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { dlg.dismiss(); }
        });

        dlg.show();
    }
}